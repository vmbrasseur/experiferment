---
title: beet-radicchio kraut
date: 2019-12-27
author: vmbrasseur
excerpt: ""
layout: single
categories:
  - kraut
---
| 451g | finely chopped red beet stems and leaves |
| 484g | shredded radicchio |
| 935g | TOTAL VEG |
| 19g | kosher salt |

2% salt, low because it's cool right now.

After thumping it, it all fit in a single quart Mason jar. Used glass weight and just a loose screw top. Fermenting downstairs, where it's currently 16C.

## 2019-12-30

Bubbled over. Tasted and there's fermentation happening. Has a nice bitter edge from the radicchio, too. Pleased so far. May use some of this juice to prime the kvass.

## 2020-01-02

No flavour difference from last tasting. Weight. It's 15.2C in that room. That's cool but not enough to stop fermentation.

## 2020-01-04

"Boiled" over again. Some good sourness developing now. I think this one will be done next week.

## 2020-01-09

Very boiled over, but it was worth it. Great colour, great taste, just the right level of sour. The bitterness of the radicchio is a great addition to the kraut.
