---
title: pickled cauliflower
date: 2022-05-29
author: vmbrasseur
excerpt: ""
layout: single
categories:
  - pickles
---

|  1227 g | cauliflower, broken into small florets |
|         | juice from 1 lemon |
|         | zest from 1/2 a lemon, removed w/a vegetable peeler |
|     9 g | black mustard seeds |
|     6 g | celery seeds |
|     5 g | coriander seeds |
|     2   | cinnamon sticks |
|    11 g | fresh tumeric, chopped |
|    2.5% | brine |

<img src="/assets/images/20220529-cauliflower.png" alt="A 5 litre fermentation crock" title="A 5 litre fermentation crock" style="width:250px; float: left; margin-right: 5px;" />

As I flipped through a cookbook this week I came across a recipe for a cauliflower vinegar pickle. It sounded good so I figured I'd do a proper ferment on it instead.

I diverged from the recipe a fair bit, but the flavourings selected ought to go well. My one concern is that there may be too much cinnamon. Time will tell.

I'd intended to put everything in a 2 litre mason jar, but after preparing it all I decided it was the perfect mixture to give my new-to-me fermentation crock its maiden voyage. I poured everything in, topped it with the fermentation weights, covered the lot with the brine, then put the top on and filled the water trench with star-san'd water that I had on hand from sanitising the fermenter.

## 2022-07-23

This was a winner. The lemon and cinnamon are distinct but still only in the background. The pH of 3.6-ish gives them a nice bite. Speaking of bite, the cauliflower kept a fairly crunchy consistency. Overall this turned out really well and will be great with a mezze, charcuterie plate, or just whenever.

I parceled it out into 4 1-pint jars.

This was the first outting for the fermentation crock and it worked like a charm. Really pleased with it!
