---
title: dill pickles
date: 2023-09-03
author: vmbrasseur
excerpt: ""
layout: single
categories:
  - pickles
---

I didn't weigh anything this time around.

* Fresh dill fronds
* Several cloves of fresh garlic, peeled
* Black mustard seeds
* Black peppercorns
* Several whole cloves
* Two fresh bay leaves from the tree in the yard
* Pickling cucumbers
* 3% brine

Placed all the herbs/spices in the bottom of my smallest fermentation crock, packed in some cukes on top of that, hold it down with the included weight, pour in the brine and make sure everything is well covered by it.

The crock sits on my kitchen counter, so I'll be able to keep an eye on it. The last time I did this, I let things ferment too long so everything tasted great but had a soft texture. Hopefully that won't happen again.

## 2023-09-12

Checked in on them today. There was a layer of kahm yeast over the top. I scraped most of that off, mostly to make it easier to take a pH reading. That ended up at 4.6. Gonna give it a few more days to try to get that number into the high 3s maybe. I won't let it go too much longer though, since the pickles will get mushy (like last year).

## 2023-09-30

pH still at 4.6. Put these into a couple of pint jars in the fridge, since I don't want the pickles to get any softer. I _think_ they're a success? Better than my first attempt, I think.
