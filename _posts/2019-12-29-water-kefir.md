---
title: water kefir
date: 2019-12-29
author: vmbrasseur
excerpt: ""
layout: single
categories:
  - drinks
---
Got some kefir grains from Yemoos and have been getting them started and established. On the fourth batch now.

## first batch

Basic "get established" batch with 2 Tbl each white and muscovado sugar. Garins started out dry and plumped up. Kefir was OK but boring and still a bit sweet.

## second batch

Another basic to get the grains going. Used bottled water this time. Grains loved it and multiplied a lot. Flavour about the same as #1 but happy grains so it's time to branch out.

## third batch

Quarter cup local wildflower honey, a couple slices fresh ginger, a couple prunes, all the grains, bottled water. Mixed then put in the fridge because I was leaving on a trip. Tasted 4 days later straight from the fridge and it was great! Not too sweet but floral and complex.

## fourth batch

Same ingredients as above. Seems to be pretty active even after only a day.
