---
title: cabbage-jalapeño-scallion kraut
date: 2020-03-15
author: vmbrasseur
excerpt: ""
layout: single
categories:
  - kraut
---

<img src="/assets/images/20200315-kraut.jpg" alt="A plastic container filled with a mixture of cabbage, jalapeños, and scallions" title="A plastic container filled with a mixture of cabbage, jalapeños, and scallions" style="width:250px; float: left;" />

| 561 g | finely shredded Savoy cabbage |
| 694 g | finely shredded green cabbage |
| 178 g | jalapeño, halved, seeded, finely sliced crosswise |
| 170 g | scallions, white and greens, finely sliced |
| 1603 g | TOTAL VEG |
| 40 g | kosher salt |
| enough | spring water |

2.5% salt, low because it's cool right now.

Thumped the cabbage with the salt, then mixed in the jalapeños and scallions.

Packed into a 1 gallon Cambro. It filled about half the container. 

Put the pottery weight on top, then added spring water to bring the liquid to the level of the weight. The cabbage was older so it didn't produce much juice when thumped.

Closed the Cambro with a green Cambro top with a hole drilled for a grommet and fermentation airlock.

Firsts for this batch:

* First time using the pottery weight since Fuzzy made them for me. It looks like I'd need to do another 500g of cabbage or whatever to get the Cambro to the level where it wouldn't need a water top-up in order to use the pottery weights.
* First time doing the water top-up. I don't _think_ it'll be a problem, but it may have thrown off the salt levels? Anyway, I think it'll be OK.
* First time using the drilled Cambro top + fermentation airlock. Unless some mold snuck in there, this should work OK. No problem anticipated.

Placed downstairs to ferment. It's currently about 3-4° Celsius down there.

## 2020-03-18

I happened to be in the ferment room and checked in on this. It's about 7C in the room right now. There's a definite "something's fermenting" aroma in there. The kraut juice has gone cloudy, so the bacteria are definitely working. No mold so far. I wasn't going to taste test this for 2 weeks but at this rate I may need to try it sooner than that.

## 2020-03-22

<img src="/assets/images/20200315-kraut-2.jpg" alt="A close-up of a very bubbly ferment" title="A close-up of a very bubbly ferment" style="width:250px; float: left; margin:10px;" />

Finally got around to checking this. Everything is going great! As shown in the picture, it's bubbling like crazy. The juice doesn't taste very acidic yet, but it does otherwise taste pleasant. Same with the kraut itself. Curiously, it's not spicy at all. Maybe next time I should consider leaving the jalapeño seeds in? The scallions are adding a good dimension to the mix. Overall the balance is great and everything's tasting fresh. It just needs a lot more zing.

I mixed everything up again really well to distribute the bacteria more evenly. Plunked the top back on it and took it back down to the fermentation room. I don't plan to check on this again for another week, at which point it should have a really good zing and be ready to put into jars (I hope).

## 2020-03-28

<img src="/assets/images/20200315-kraut-3.jpg" alt="Extreme closeup of the finished kraut" title="Extreme closeup of the finished kraut" style="width:250px; float: left; margin:10px;" />
<img src="/assets/images/20200315-kraut-4.jpg" alt="Four wide-mouth pint jars filled with kraut" title="Four wide-mouth pint jars filled with kraut" style="width:250px; float: left; clear:right; margin:10px;" />

And it's done! It has a nice tang to it and currently is slightly fizzy. Surprisingly, it's not spicy at all, but a few minutes after tasting it I can tell there's some capsaicin in there somewhere as there's a gentle warmth on my tongue. I think that's more thanks to the jalapeños I used (which were probably not very spicy) than the ferment. The scallions were a great idea and add a lot of flavour & body to the mix.

Using the spring water to top off the ferment ended up working really well. For the first time I had plenty of kraut juice. Usually my krauts are quite dry no matter how much I thump them. I'm definitely going to do the spring water thing again.

The hardware all worked well, too. The fermentation lock kept everything clean without exploding. The pottery weight kept it all submerged. The one change I'd make here is to make sure to make a larger batch if I'm going to use the pottery weight. After checking the batch last weekend I kind crammed the weight down further than I should have, wedging it in there and making it difficult to remove today. That wouldn't have been a problem had the kraut batch been just a little larger.

I packed the kraut into four wide-mouth pint jars then topped each jar up with the remaining juice from the fermenter. I'm about the move them to the fridge. My suspicion is that I over-filled one or two with the juice and even with refrigeration I may end up with a bit of bubble-over in the fridge. Eh, no biggie. I have glass shelves so they're easy to clean.
