---
title: white kraut
date: 2022-12-03
author: vmbrasseur
excerpt: ""
layout: single
categories:
  - kraut
---

|  1351 g   | finely sliced green cabbage |
|   285 g   | grated purple-top turnip |
|   279 g   | grated parsnip |
|   **1915 g** | **TOTAL VEG** |
|    48 g   | salt (2.5%) |
|     2.5%    | brine to cover |

I've been wanting a non-red kraut for a while. At some point I ended up with a green cabbage, a large turnip, and a couple of parsnips that I hadn't used for their intended purposes. And thus this white kraut was born.

Because all of the veggies were on the older side, they were relatively dry. Even after a long thumping they didn't create enough liquid to cover so I added a brine to compensate.

I really like how spicy the turnip and parsnip make the mix smell, so I'm looking forward to the end result of this one.

## 2023-01-07

<img src="/assets/images/20221203-kraut.png" alt="Six wide-mouthed pint canning jars filled with a yellow-white kraut, standing on a folded up kitchen towel atop a dark granite countertop" title="Six wide-mouthed pint canning jars filled with a yellow-white kraut, standing on a folded up kitchen towel atop a dark granite countertop" style="width:250px; float: left; margin-right: 5px;" />

I probably could've put this one up a week ago, but I didn't feel like doing it over the holiday. The kraut didn't care. Ready last week or ready today; it's still ready.

The pH today came to 3.7, which is supported by the good, sour bite on it. The flavour is pretty good! And the vegetables retained much of their crunch. I need to give it a more focused taste later, but my initial impressions are that this is the lighter-coloured kraut I've been wanting.
