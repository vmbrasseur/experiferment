---
title: escabeche
date: 2023-10-31
author: vmbrasseur
excerpt: ""
layout: single
categories:
  - pickles
---

|    73 g   | jalapeños (2), sliced into rings |
|   119 g   | onion (1/2), sliced into half rings |
|    94 g   | carrot (1), sliced into rings |
| **286 g** | **TOTAL VEG** |
|     4     | dried chilies de arbol |
|           | a loose-leaf tea bag filled with dried Mexican oregano |
|           | 3% brine to cover |

I mixed up the veg, packed them into a sanitised 1 quart mason jar, then slid the dried chilies in along the sides. Placed the tea bag of oregano on top, held it all down with a spring weight, and covered with the brine.
