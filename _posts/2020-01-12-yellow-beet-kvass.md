---
title: yellow beet kvass
date: 2020-01-12
author: vmbrasseur
excerpt: ""
layout: single
categories:
  - drinks
---
<img src="/assets/images/20200112-yellowbeetkvass.jpg" alt="A mason jar filled with a new batch of kvass made with yellow beets" title="A mason jar filled with a new batch of kvaas made with yellow beets" style="width:250px; float: left;" />

| 1 medium | diced yellow beet |
| to fill | spring water |
| large pinch | salt |

## 2020-01-15

Tasted and it's obviously starting to ferment a bit. It's a subtle change, but promising. I shook up the jar to make sure the bacteria are distributed and not just lurking around the beet bits.

## 2020-01-18

Tasted. It has a bit of acid zing and doesn't taste as salty. Strained it out, put it in a plastic bottle, and put it in the fridge.
