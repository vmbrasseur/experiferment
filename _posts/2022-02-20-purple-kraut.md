---
title: purple kraut
date: 2022-02-20
author: vmbrasseur
excerpt: ""
layout: single
categories:
  - kraut
---

<img src="/assets/images/20220220-kraut.png" alt="A 2 quart Mason jar about 2/3 full of the kraut mixture, topped with an airlock" title="A 2 quart Mason jar about 2/3 full of the kraut mixture, topped with an airlock" style="width:250px; float: left; margin-right: 5px;" />

|      535 g   | finely sliced red cabbage |
|      568 g   | grated purple daikon |
|      198 g   | finely sliced red onion |
|   **1301 g** | **TOTAL VEG** |
|       39 g   | salt (3%) |

The previous kraut is almost all gone, so it's time to start another one. I haven't done a red cabbage kraut in a long time so it's the way I went this time. The store had purple daikon (which I don't recall seeing before), so it obviously needed to go in. While I was at it, I added a red onion to give it a nice flavour.

## 2022-04-02

<img src="/assets/images/20220220-kraut-2.png" alt="Electric pink kraut in a large stainless steel bowl" title="Electric pink kraut in a large stainless steel bowl" style="width:250px; float: left; margin-right: 5px;" />
<img src="/assets/images/20220220-kraut-3.png" alt="Three wide-mouth pint jars filled with hot pink kraut" title="Three wide-mouth pint jars filled with hot pink kraut" style="width:250px; margin-right: 5px;" />

A little over a month later and this kraut is _very_ done. I didn't pull out the pH metre, but it's more sour than my usual kraut and I kinda love it.

It turned out a brilliant pink. It's really striking and, again, I kinda love it.

This batch made just shy of three pints of kraut. Even if I give one of the pints away, I'll still have plenty to keep me going for a while.
