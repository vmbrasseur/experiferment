---
title: dill green garlic spears 
date: 2022-05-15
author: vmbrasseur
excerpt: ""
layout: single
categories:
  - pickles
---

| 3 bunches | green garlic, trimmed of greens |
|     13 g  | sprigs of fresh dill |
|      2    | achiote seeds |
|      3%   | brine |

<img src="/assets/images/20220515-greengarlic-1.png" alt="A 1 quart mason jar packed with green garlic spears" title="A 1 quart mason jar packed with green garlic spears" style="width:250px; float: left; margin-right: 5px;" />

A vendor at today's farmers market had green garlic, so I picked up a few bunches for fermentation experiments.

I trimmed the roots off each garlic, washed them, then trimmed them down to the height of a 1 quart wide mouth mason jar.

I happened to have dill on hand for a dish I'm cooking later today, so I decided to add that to the bottom of the jar, along with 2 small achiote seeds to add some colour to the ferment.

Then I packed the spears tightly into the jar, filled with 3% brine, tapped to release any bubbles, then added a fermentation weight and an airlock top.

## 2022-07-23

Checked today. This batch has a pH of 3.7 but doesn't taste especially acidic. It also doesn't have much of an odor, which is odd considering it's dill and garlic. I ate a spear and it was just OK. Kinda dull, really.

I put a sanitised top on the jar and put the lot into the fridge.

