---
title: "blackberry melomel"
date: 2015-07-13
author: vmbrasseur
excerpt: ""
layout: single
categories:
  - melomel
  - drinks
---

| 1 gal | organic apple juice |
| 1 large pint | local blackberries |
| ~12 oz | local honey |

Mix all. Cover. Stir once in a while.

Put into second fermentation after ~10 days.

Bottled on August 3rd, 2015.

Results: This turned out great! I was aiming for something which would retain some residual sugar after the yeast finished and I nailed it. There's not really any blackberry flavour, but it does have a slight pink tinge to it. I put it in swing-top bottles and am going to see whether it'll lightly carbonate. I may try a bottle in a week or so.
