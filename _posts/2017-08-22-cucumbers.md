---
title: cucumbers
date: 2017-08-22
author: vmbrasseur
excerpt: ""
layout: single
categories:
  - pickles
---
| 1291g | whole cukes of various types and sizes (from garden) |
| 51g | salt (4% solution) |
| 48g | whole unpeeled garlic cloves |
| 5g | whole dill seed (all I had on hand) |
| 6g | whole coriander seed |
| 2g | whole black peppercorn |
| 3g | whole dried Thai chilis |
| 2 | whole fresh Californian bay leaves |

Started as 4% salt by cuke weight then was far too light a brine. I did the wrong thing after that and eyeballed it, testing brine until it was decently salty. Used zippy filled with brine as weight. Placed in sunny spot.

UGH!! MOLD!!
