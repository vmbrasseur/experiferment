---
title: green chili sauce
date: 2021-02-06
author: vmbrasseur
excerpt: ""
layout: single
categories:
  - sauces
---

<img src="/assets/images/20210206-chilies.jpg" alt="1/2 pint wide mouth mason jar half-filled with sliced chilies and brine" title="1/2 pint wide mouth mason jar half-filled with sliced chilies and brine" style="width:250px; float: left;" />

| 82 g | serrano and jalapeño chilies, sliced (incl. seeds) |
|  8 g | crushed garlic |
| 2.5% | brine |
| 53 g | kraut juice |

I had a bunch of chilies that I wasn't going to use any time soon, so I figured I'd try my hand at a small batch of chili sauce. I'd like something a little thicker than the [first batch I tried](https://experiferment.vmbrasseur.com/sauces/hatch-chili-sauce/), and with a lot more (read: any) kick.

This is also a way to prove to myself that I don't _need_ to do big batches when I ferment. Small is OK.

I used the kraut juice partly because I had it on hand but mostly because I wanted to use the jar it was in to make this ferment.

Sliced and mixed everything up, packed it into the sanitised jar, topped with the kraut juice and brine, then used a sanitised kraut spring and fermentation top to submerge everything below the brine level.

## 2021-02-20

There was a ring of mold at the top of the ferment when I opened the jar. I carefully pulled that out, then removed the kraut spring and rinsed it off under super hot water from the tap.

The fermenation room was **13.3 C** when I retrieved everything. It's been pretty cold around here lately. That's likely slowed the ferment a bit.

The pH on this is currently **3.9**. I want it lower (maybe 3.5?).

I didn't taste the mix because I didn't feel like dealing with the chilies' heat right now.

I'll check it again in two weeks and expect it'll be done by then.

## 2021-03-06

There was another ring of mold at the top, but it was thin and exclusively on the kraut ring so it was easy to remove.

Final pH was **3.5**.

The flavour is spicy, as expected. It could use some more depth, but it's a good start for an experiment.

Strained the chilies & garlic, saving the juice. Put the chilies, garlic, a fresh garlic clove, and some of the juice through the Vitamix on high. **Memo to self:** When you do this and you stop the blender to check on progress, do **not** put your face right over the blender jar after removing the top. Just…don't. In related news, my sinuses are clear now.

Anyway, did that then put the blend through a fine-mesh strainer. There was very little that didn't make it through the mesh, so the Vitamix did its job well (which is why I bought it). The end product has spice and zing and is pretty good! Should be nice on eggs or nachos or whatever.

I'd intended to add fresh cilantro to the final product (for flavour and to boost the green), but unfortunately the bunch I'd bought had turned.
