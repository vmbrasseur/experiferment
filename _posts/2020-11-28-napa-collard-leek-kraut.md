---
title: napa cabbage, collard green, leek kraut
date: 2020-11-28
author: vmbrasseur
excerpt: ""
layout: single
categories:
  - kraut
---

<img src="/assets/images/20201128-kraut.jpg" alt="2 liter Cambro container packed tightly with nascent kraut in varied shades of green, held down with pottery weight." title="2 liter Cambro container packed tightly with nascent kraut in varied shades of green, held down with pottery weight." style="width:250px; float: left;" />

| 1005 g | finely sliced napa cabbage |
| 176 g | finely sliced collard greens |
| 359 g | finely sliced leeks, white part only |
| 1540 g | TOTAL VEG |
| 31 g | kosher salt (2% weight of veg) |

I chose these veggies because they looked good when I was in the store. Plus I wanted something that would be moderately neutral in flavour. I really liked the [cabbage-jalapeño-scallion kraut](/kraut/cabbage-jalapeno-scallion-kraut/), but that jalapeño made it less appropriate to use in all cases.

The collards & leeks were weighed after slicing but before they got a very good rinse. Each had a fair bit of dirt on them.

I mixed the collards and leeks first and really liked the way it looked and smelled. I may need to do a collard-leek kraut sometime soon, skipping the cabbage entirely.

<img src="/assets/images/20201128-kraut-2.jpg" alt='Very large pottery bowl filled with shredded vegetables. Across the top of bowl is a wooden rolling pin for thumping the veg. A small bowl of kosher salt sits to one side.' title="Very large pottery bowl filled with shredded vegetables. Across the top of bowl is a wooden rolling pin for thumping the veg. A small bowl of kosher salt sits to one side." style="width:250px; float: right;" />

However, cabbage was the name of the game today so I added it in. The mixture of different greens and whites in the bowl was really pretty.

There was plenty of juice after the thumping so I didn't need to supplement with any 2% brine.

The kraut is very well packed into the 2 litre Cambro. Like for the [Hatch chili sauce](/sauces/hatch-chili-sauce/), I find that the top isn't sealing and will leak if I tip the container. This is no good. I'll need to test the 2 litre Cambros later to see whether it's them or the top.

It was 15C in the fermentation room when I moved the kraut down there today.

## 2020-12-05

<img src="/assets/images/20201128-kraut-3.jpg" alt="A Cambro container sitting in a puddle of kraut juice" title="A Cambro container sitting in a puddle of kraut juice" style="width:250px; align: left; margin-right: 10px;" />

Kraut-splosion! Well, OK, not that bad, but it looks like things got very active and bubbled a lot of the kraut juice out of the container.

What this tells me is that…

1. I really need to check on why the Cambro top isn't sealing, and
1. I also really need to stop cramming the kraut into a container juuuuuuust barely large enough for it.

Anyway, I tasted it and it's progressing pretty well. There's a very strong leek flavour, which I rather like but I'm an onion fan. There's no acid to speak of really, but it's just a matter of time.

Despite the blow-off of juice, there was no mold in sight, which was a relief. I didn't add more brine to make up for the lost juice, since I figure it's just gonna bubble out again.

Overall, I think this batch is going very well.

## 2020-12-12

No kraut-splosion this time, which makes sense since the kraut is relatively dry now after the last one.

There was no smell from the outside of the Cambro, but it smelled good when I opened it. There's a slight tang to the odor now, which is matched in the flavour. It's developing a good bite. The leek flavour is still the strongest one, but I like it. The cabbage gives it a nice crunch. I'm really happy with how this is working out so far.

It needs at least one more week, maybe two if I want it to have a sharp tang. I'll see how it tastes next week then make the call.

## 2020-12-26

<img src="/assets/images/20201128-kraut-4.jpg" alt="Two and a half wide-mouthed pint jars filled with kraut" style="width:250px; align: left; margin-right: 10px;" />

Kraut Kompletion! It's not super zingy but it has a nice tang to it. It's going to be very tasty on sandwiches and other savoury things.

Today marked the inaugural outting for my Shiny! New! pH meter! that I received for Xmas yesterday. According to my new toy, the pH of this kraut is 3.7. That's an acidity that's [just below tomato juice](https://www.sciencebuddies.org/science-fair-projects/references/acids-bases-the-ph-scale), which seems about right for what I'm tasting. Now that I have this toy^wtool I'll have to do more research into acid in ferments.

I was pleased to see that despite the earlier kraut-splosion, the finished product wasn't dry. There's not a ton of juice, but there's enough that a light press on the top of the kraut in the jars will easily submerge it. That's a decent enough amount.

The batch filled 2.5 pint jars. One of them is destined for Fuzzy & Kris. The others will stay here for on my morning toast & hummus. I've been missing the kraut kick on my breakfast.
