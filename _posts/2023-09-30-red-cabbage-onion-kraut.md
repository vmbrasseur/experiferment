---
title: red cabbage and onion kraut
date: 2023-09-30
author: vmbrasseur
excerpt: ""
layout: single
categories:
  - kraut
---

<!-- <img src="/assets/images/" alt="" title="" style="width:250px; float: left; margin-right: 5px;" /> -->

| 110 g   | sliced young red onions (from garden) |
| 504 g   | shredded red cabbage |
| **614 g** | **TOTAL VEG** |
|     4 g   | black mustard seed |
|     1 g   | caraway seed |
|    19 g   | salt (3.0%) |
|    3.0%   | brine to cover |

