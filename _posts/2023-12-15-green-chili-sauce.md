---
title: green chilli sauce
date: 2023-12-15
author: vmbrasseur
excerpt: ""
layout: single
categories:
  - sauces
---

| 203 g | sliced fresh poblanos |
|  51 g | sliced fresh serranos |
| 156 g | sliced fresh jalapeños |
| 286 g | tomatillos, sliced into sixths |
|  80 g | garlic cloves |
|  3.0% | brine |

I had a bunch of stuff sitting around that needed using, so I figured I'd put together a ferment so the ingredients wouldn't go to waste.

Because I also had a pineapple that I intended to juice today, I sliced off the bottom and used it to hold the chilis under the brine. It may add a nice fruity edge to the eventual sauce.

The lot is now resting on the counter in a 2 liter mason jar, topped with a fermentation lock.

## 2024-01-13

Today was bottling day. The fermentation long since stopped bubbling and the brine had cleared up, all the bacteria detritus having fallen to the bottom. 

It had a pH of 4.4, confirmed by a taste. There's a lotta acid kick there. There's also a lotta heat, which was expected.

I strained the lot (saving the brine) and put it—pineapple bottom and all—in the Vitamix along with a roughly chopped bunch of cilantro. Blitzed the crap outta it, adding brine to keep things moving and get it to a saucy consistency. The Vitamix did its job admirably, leaving no chunks that needed straining out afterward.

It made five half pints of green chilli sauce. Four of them went into mason jars thence into the fermentation fridge, and one into a squeeze bottle and into the main fridge for use.

Initial taste tests are positive, but it's gonna need to meld a bit to integrate that cilantro. It also has a bitter edge to it, probably from the seeds in the chillies. Overall though, I think it's a success. And I won't be needing to buy hot sauce for a long time.
