---
title: savoy cabbage and leek kraut
date: 2023-10-29
author: vmbrasseur
excerpt: ""
layout: single
categories:
  - kraut
---

<!-- <img src="/assets/images/" alt="" title="" style="width:250px; float: left; margin-right: 5px;" /> -->

|   1180 g   | shredded savoy cabbage |
|    243 g   | finely sliced leek
| **1423 g** | **TOTAL VEG** |
|     43 g   | salt (3.0%) |

Today was the last farmers market for the year, and it also happens to be cabbage season, so I was able to get a fresh savoy and some fresh leeks.

Fresh makes such a difference. Most grocery store cabbages have been sitting around so long that they don't have a lot of moisture. With today's veg, for probably the first time, I was able to get enough juice brine just from thumping.

After packing everything down well, it about 2/3 filled a 2 litre mason jar. I weighed everything down with a glass weight on top of a whole savoy leaf, then screwed on an airlocked top.
