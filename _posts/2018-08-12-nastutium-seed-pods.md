---
title: nasturtium seed pods
date: 2018-08-12
author: vmbrasseur
excerpt: ""
layout: single
categories:
  - pickles
---
| 51g | seed pods, washed and dried |
| 2g | kosher salt |
| 76g | water |

Submerge in a jar, held down by a ziptop bag containing brine of same strength.

## 2018-08-13

Plenty of little bubbles in the brine, so fermentation is moving.

## 2018-08-14

Lots of bubbles. Smells a bit sulfurous when I really get close and sniff.

## 2018-08-21

First taste test. A small bit of mold on the brine bag. The pods don't taste particularly interesting. Very little mustardy bite or acid at all. Gonna pitch them. Not worth the effort to try again, IMO.
