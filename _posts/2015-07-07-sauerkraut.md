---
title: sauerkraut
date: 2015-07-07
author: vmbrasseur
excerpt: ""
layout: single
categories:
  - kraut
---

| 1289g | red cabbage, shredded |
| 24g   | salt                  |
| 6g    | caraway seeds         |
| 4g    | celery seeds          |

Give a good thumping. Weight down with brine bag. Cover and set aside.

10 days later -> done.

Results: Much dryer than last batch (more thumping needed). Not a huge fan of the celery seeds, but the caraway is nice. A lovely shade of magenta. OK acidity (could use more) and good crunch.
