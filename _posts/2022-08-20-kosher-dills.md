---
title: kosher dill pickles
date: 2022-08-20
author: vmbrasseur
excerpt: ""
layout: single
categories:
  - pickles
---

Spice mix:

| 5 g | whole allspice |
| 15 g | whole black mustard seed |
| 3 g | whole cloves |
| 5 g | cinnamon stick (4" stick, roughly crushed) |
| 5 g | whole black peppercorns |
| 0.5g | crumbled dried bay leaves |
| 5 g | whole coriander seeds |
| 5 g | whole dried chile de arbol |

Pickles: 

|  3631 g | whole pickling cucumbers of various small shapes and sizes |
|    63 g | peeled whole garlic cloves |
|    61 g | dill flower heads |
|    97 g | dill stems |
|   118 g | backslop from [brussels sprouts, collards, leek kraut](https://experiferment.vmbrasseur.com/kraut/sprouts-collards-leek-kraut/)
|    3.0% | brine to cover |

<div id="pics" style="width:100%">
<img src="/assets/images/20220820-kosherdills-1.png" alt="A clear plastic bag with pickling cucumbers spilling out. Atop the bag leans a bunch of fresh dill." title="A clear plastic bag with pickling cucumbers spilling out. Atop the bag leans a bunch of fresh dill." style="width:150px; float: left; margin-right: 5px;" />
<img src="/assets/images/20220820-kosherdills-2.png" alt="Pickling spices and some dill flower heads inside a stoneware fermenting crock" title="Pickling spices and some dill flower heads inside a stoneware fermenting crock" style="width:150px; float: left; margin-right: 5px;" />
<img src="/assets/images/20220820-kosherdills-3.png" alt="Stoneware weights on top of cucumbers in the fermenting crock" title="Stoneware weights on top of cucumbers in the fermenting crock" style="width:150px; float: left; margin-right: 5px;" />
<img src="/assets/images/20220820-kosherdills-4.png" alt="The 3 gallon stoneware fermentation crock sitting on the floor in a corner of the kitchen" title="The 3 gallon stoneware fermentation crock sitting on the floor in a corner of the kitchen" style="width:150px; float: left; margin-right: 5px;" />
</div>

Dill pickles have dill. Kosher dill pickles have dill and garlic. Therefore, these are kosher dill pickles, though it's unlikely a rabbi will have the chance to confirm that.

I was at the Beaverton farmers market today and a small booth had several crates of pickling cucumbers available. I'd thought I'd missed these this season, so I picked up about eight pounds of them. Another booth had long-stemmed dill flowers for pickling, so I picked those up as well then went home to do the needful.

This is the first outing for my new-to-me three-gallon fermentation crock. I placed 1/2 of the spices, garlic, and dill flower heads at the bottom of the sanitised crock, added 1/2 the cucumbers, added the rest of the spices, garlic, and flower heads then the rest of the cucumbers. I'd cut the dill stems to fit the diameter of the crock then layered them over the cucumbers. My hope is they'll not only add flavour but also keep the whole spices from floating to the top.

After weighing everything down with the also new-to-me fermentation weights sized to fit this crock, I added several litres of 3% brine.

The plan was to keep this down in the fermentation room (read: space under the basement stairs), but the crock is heavy when empty. When half full of cukes and brine, there was no way it was getting down the stairs safely. So it'll be living in a corner in my kitchen for the next couple of months. At least it'll be easy to check?
