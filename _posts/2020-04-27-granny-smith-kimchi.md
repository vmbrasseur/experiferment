---
title: granny smith kimchi
date: 2020-04-27
author: vmbrasseur
excerpt: ""
layout: single
permalink: /2020/04/27/granny-smith-kimchi
categories:
  - kimchi
---
<img src="/assets/images/20200427-kimchi.jpg" alt="A plastic container filled with a fresh batch of kimchi, ready to ferment" title="A plastic container filled with a fresh batch of kimchi, ready to ferment" style="width:250px; float: left;margin-right: 1.5em;" />
What would happen if I replaced the normal daikon in my recipe with granny smith apples? Let's find out…

| 1.303 kg | Taiwan cabbage, cut to ~3cm squares |
| 601 g | granny smith apple, unpeeled, halved, cored, sliced to ~3mm thick |
| 220 g | scallions, white and greens, cut to ~1cm pieces |
| 2.124 kg | TOTAL VEG |
| 15% | brine solution |
| 200 ml | water |
| 50 ml | brown rice flour |
| 91 g | chopped garlic |
| 162 g | grated ginger |
| 100 g | Korean red pepper flakes |

Soak veg in a 15% brine solution for 3 hours. Rinse and squeeze/drain well.

Slurry from rice flour and water. Cook, cool.

Mix veg, slurry, spices. Innoculate with the last of the previous batch.

Packed into 1 gallon Cambro. Weighed down with pottery weight. Topped w/drilled Cambro top and a fermentation airlock.

Placed in the basement storage room, where it's currentl 15.5C and 62% humidity (we've had a lot of rain lately).

## 2020-05-04

Peeked in on it today. Was a little bit of mold fuzz starting to grow on some exposed veg. Scraped those off, cleaned the pottery weight. Tasted while I was at it. As expected, nothing much going on there yet. It'll take a while before the fermentation is obvious. Pushed everything down well to submerge, put pottery weight back on, sealed up again.

Still around 15C in the fermentation room.

## 2020-05-09

A bit more mold on the top. Scraped it off, no biggie. Smelling very good. It's starting to become kimchi. Flavour is good but still not there. Too much salt, not enough tang. The garlic is so strong that it's wafted out into the den from the fermentation room. I smell it every time I go downstairs.

Gonna check again in 4 days, if only to make sure the mold doesn't get a foothold.

About 17C in the fermentation room today, but it's about 30C outside.

## 2020-05-17

I've been peeking in all week and spotted no mold. Plenty of liquid on the top, which probably helped.

Stirred it up and tasted today. It's going really well! It has good bite and heat, but needs a bit more complexity. I'm going to let it sit another week or two before putting in jars.

About 16C in the fermentation room today. It's been ~15C outside and fairly rainy.

## 2020-05-24

Stirred and tasted again. It's developed a bit in the past week but I think it could still use some more. I'll give it another week or two.

It was 15.2C in the fermentation room when I checked. Today's the first non-rainy day in about 3 weeks and next week promises to warm up again.

## 2020-06-06

<img src="/assets/images/20200427-kimchi-2.jpg" alt="A tower of six half-litre jars of finished kimchi" title="A tower of six half-litre jars of finished kimchi" style="width:250px; float: left;margin-right: 1.5em;" />

Not a massive difference from last time, but enough. Put it in jars today. It made 6 1/2 litre jars.

Capped loosely then put in the fridge to slow fermentation down to a creep.
