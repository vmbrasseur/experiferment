---
title: hatch chili sauce
date: 2020-09-19
author: vmbrasseur
excerpt: ""
layout: single
categories:
  - sauces
---

<img src="/assets/images/20200919-hatch-chilies.jpg" alt="2 liter Cambro container with sliced Hatch chilies and a 1.5% brine, held down with pottery weight." title="2 liter Cambro container with sliced Hatch chilies and a 1.5% brine, held down with pottery weight." style="width:250px; float: left;" />

| 547 g | Hatch chilies, sliced (incl. seeds) |
| 20 g | chopped garlic |
| 1.5% | brine |
| 54 g | juice from [cabbage-jalapeño-scallion kraut](https://experiferment.vmbrasseur.com/kraut/cabbage-jalapeno-scallion-kraut/)

It's [Hatch chili](https://en.wikipedia.org/wiki/New_Mexico_chile) season, so it's time I finally tried making a fermented chili sauce.

Only a 1.5% brine on this one, since the fermentation room (read: storage room in the basement) is proving good at keeping a pleasant temp (sub-19C). Also, most of our summer heat has now passed. Also also, I didn't want the final product too salty. It's easier to add salt later should that be needed.

I added kraut juice on a whim. It's not needed, but won't harm anything. The flavour profile matches well, but it'll be lost in the chilies anyway. Mostly it'll just give a little kickstart to the process, knocking a few days off the ferment.

## 2020-09-26

Quickly peeked in on the chilies today. They're chugging along nicely. Brine is pleasantly cloudy so the bacteria are doing their job. Zero mold in sight. You can smell them and the garlic from outside the Cambro, but that's probably because the lid won't seal tightly for some reason. That worries me a little bit, but they're all submerged in the brine so they should be OK for the duration. I'll check again next weekend to have a look for mold, just in case.

## 2020-10-24

<img src="/assets/images/20200919-hatch-chilies-2.jpg" alt="Several small glass jars filled with chili sauce and one filled with leftover brine" title="Several small glass jars filled with chili sauce and one filled with leftover brine" style="width:250px; float: left; margin-right: 10px;" />Finally getting back to these. The ferment is nice and sharp. Zero mold anywhere in the container. Really pleased with how that part went.

I strained the chilies then started in on them with the immersion blender. The poor blender isn't meant for this sort of work (and the chili skin is pretty tough), so I had to let it rest to cool off a few times.

Eventually I pulled out the food mill and put the blended chilies through it. This resulted in a very saucey sauce, with no chunks. It has good chili flavour but pretty much zero heat at all.

All told it made about a pint of sauce. I split it into three small jars since I promised some to a couple of friends. I also saved off a small jar of the brine since it's also pretty tasty. I may use it to start a batch of something else.

Overall I'd say this was a success. Next time I might mix in a couple of spicy chilies to add a bit more interest. Or maybe a single habañero for the whole batch. Some herbs (cilantro) and/or lime might also be nice.
