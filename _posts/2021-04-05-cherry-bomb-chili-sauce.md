---
title: cherry bomb chili sauce
date: 2021-04-05
author: vmbrasseur
excerpt: ""
layout: single
categories:
  - sauces
---

<img src="/assets/images/20210405-chilies.jpg" alt="1 litre wide mouth mason jar filled with sliced red chilies and brine" title="1 litre wide mouth mason jar filled with sliced red chilies and brine" style="width:250px; float: left;" />

| 368 g | cherry bomb chili peppers, quartered (incl. seeds) |
|  10 g | chopped garlic |
|   8 g | chopped dried chipotle chili pepper |
|  17 g | chopped dried New Mexico chili pepper |
|   4 g | chopped dried Arbol chili pepper |
|  2.5% | brine |

I wanted to do a red chili sauce. Cherry bombs are a great shade of red, but you only ever really see them on the olive bar at the grocery store. They're also not overly spicy, so they'd need some more kick.

Do dried chilies ferment? I mean…sure? Why not? Once they're rehydrated, anyway, or at least so I imagine. Well, there's one way to find out…

So I tossed some into the ferment. They should add depth, interest, heat.

I packed the lot into a one litre jar, topped with the brine, then weighed it down with one of the glass fermentation weights. I'm using the screwtop ferment lids with a fermentation lock. All equipment got a good bath in StarSan before use.

And if this doesn't work out? Meh, it's still worth a shot.

## 2021-05-15

<img src="/assets/images/20210405-chilies-2.jpg" alt="A small stainless steel bowl full of chili sauce" title="A small stainless steel bowl full of chili sauce" style="width:250px; float: left; margin-right:10px;" />

Finally getting back to this one.

pH of **3.5** when I checked it this morning, so this has fermented out nicely. Tasting the brine proves that. It has a nice acid kick and is definitely spicy.

Strained off the brine and put the chillies into the Vitamix along with a couple pinches of Mexican oregano and a couple medium-sized fresh garlic cloves. Blitzed the hell outta it, adding brine as needed to get a nice sauce consistency. Put the mixture through a sieve, pressing it through with a spatula. 

Turned out really well! This is by far the best consistency of any chili sauce I've yet made. The flavour is spicy with that slightly bitter back that you get from dried chillies. It's a tasty little experiment that actually worked out.

<img src="/assets/images/20210405-chilies-3.jpg" alt="Two 250ml squeeze bottles of chili sauce" title="Two 250ml squeeze bottles of chili sauce" style="width:250px; float: left;" />
