---
title: beet kvass
date: 2019-12-27
author: vmbrasseur
excerpt: ""
layout: single
categories:
  - drinks
---
| 120g | diced red beet |
| 741g | water |
| large pinch | salt |

Will be fermenting in the basement storage room, which is a fairly cool 15-16C.

## 2019-12-30

Taste test. Zero ferment so far. May need to prime them?

## 2020-01-02

15.2C in ferment room. Still no ferment, just beet and water. Suspect problem is my water (chlorine). Primed each with a little juice from the beet kraut.

## 2020-01-04

A little ferment maybe? Very light but primising. Re-innoculated with juice from beet kraut.

## 2020-01-09

Nope, this is a total flop. No change from five days ago. I'm almost positive this flopped because of the tap water. I'll have to try again soon.

## 2020-01-11

I dumped these today and found something interesting: a deep red, stringy sludge at the bottom of each. It appears fermentation was happening, just down low and not spreading to the rest of the jar. I'm not sure what to think of this. Does it mean I should shake the jars once in a while? Or was it a result of using tap water? It killed off a lot of the bacteria and left an imbalance?

I have 2 beets that I picked up yesterday. I'll be starting a new batch this weekend.
