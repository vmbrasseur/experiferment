---
title: dill green garlic relish
date: 2022-05-15
author: vmbrasseur
excerpt: ""
layout: single
categories:
  - kraut
---

|  146 g | chopped green garlic stems and greens |
|   10 g | sprigs of fresh dill |
|    1   | achiote seeds |
|    3%  | brine |

<img src="/assets/images/20220515-greengarlic-1.png" alt="A wide mouth pint size mason jar packed with chopped green garlic stems and greens" title="A wide mouth pint size mason jar packed with chopped green garlic stems and greens" style="width:250px; float: left; margin-right: 5px;" />

A vendor at today's farmers market had green garlic, so I picked up a few bunches for fermentation experiments.

There were so many stems and greens left from preparing the dill green garlic spears that I decided to make a sort of kraut/relish out of them. I chopped them into small-ish pieces, then packed them into a pint jar into which I'd already placed a sprig of fresh dill and a single achiote seed. Then I poured in the 3% brine and pressed down on the lot to get rid of the bubbles. After that I added a glass fermentation weight then an airlock top.

## 2022-07-23

When I opened this today, the tasty smell of pickled dill hit me pretty hard. The garlic bits are a little tough and slightly bitter, but overall it's not bad! It has a little bit of acid bite to it. The pH is 4.1.

I moved it to a half pint jar then put it in the fridge. It'll be good on sandwiches and the like.

