---
title: green cabbage shallot kraut
date: 2024-05-26
author: vmbrasseur
excerpt: ""
layout: single
categories:
  - kraut
---

|   432 g   | shredded green cabbage |
|   106 g   | finely sliced shallot |
| **538 g** | **TOTAL VEG** |
|    13 g   | salt (2.5%) |

Since the [last kraut](/2024/03/red-cabbage-red-onion-kraut) flopped, I need to start a new batch.

A farmer at the market had small, fresh, green cabbages, so I'm making a small batch of kraut with one of them.

## 2024-06-02

Things look to be progressing pretty well so far, but I haven't tasted it yet. I'll save that for after I return from my trip in a week-ish.

## 2024-06-??

I tried the kraut after returning from my trip and it was barely fermenting at all. Lots of salt evident, little/no acid. Popped the top back on and let it sit a while longer.

## 2024-06-30

OK, that's better. It has noticable acid, little salt. The shallots really jump out. It'll be tasty on breakfast toast & hummus.
