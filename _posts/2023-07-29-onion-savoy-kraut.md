---
title: onion savoy kraut
date: 2023-07-29
author: vmbrasseur
excerpt: ""
layout: single
categories:
  - kraut
---

<img src="/assets/images/20230729-kraut.png" alt="A quart-size mason jar a little over half full of kraut in various shades of green and flecked with dark specks of whole spice seeds. The jar has a hot pink plastic fermentation top with a fermentation airlock stuck into the hole in the middle." title="A quart-size mason jar a little over half full of kraut in various shades of green and flecked with dark specks of whole spice seeds. The jar has a hot pink plastic fermentation top with a fermentation airlock stuck into the hole in the middle." style="width:250px; float: left; margin-right: 5px;" />

|   144 g   | sliced young yellow onions, greens incl. (from garden) |
|   355 g   | shredded savoy cabbage |
| **499 g** | **TOTAL VEG** |
|     3 g   | black mustard seed |
|     1 g   | caraway seed |
|    15 g   | salt (3.0%) |
|    3.0%   | brine to cover |

## 2023-08-18

I portioned this out into half pint jars and put them in the fridge today. The cabbage is a perfect crunchy-squeaky, the acid is pretty high and zingy, the mustard adds a pleasant little heat, and the caraway kinda got lost. Still: it's really tasty and therefore a big success.
