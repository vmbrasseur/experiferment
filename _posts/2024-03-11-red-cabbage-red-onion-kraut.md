---
title: red cabbage red onion kraut
date: 2024-03-11
author: vmbrasseur
excerpt: ""
layout: single
categories:
  - kraut
---

|   1124 g   | shredded red cabbage |
|    222 g   | finely sliced red onion |
| **1346 g** | **TOTAL VEG** |
|     67 g   | salt (2.5%) |

I'm nearly out of the [savoy cabbage kraut](/2023/10/savoy-leek-kraut), so it's time to start another kraut batch.

I happened to have a red cabbage in the fridge, so a simple red kraut it is.

## 2024-04-06

Well, that's odd. I figured that nearly a month on at kitchen temperature, this would be _more_ than done.

But when I started putting it into pint jars for storage/use, I found it was still salty and had little acid. The veg wasn't spoiled, but also not really fermented.

I put it into the jars anyway and am leaving them on the kitchen counter for now. I'll test again in another month maybe?

## 2024-05-21

I have no idea what's going on here. The kraut is still salty, unspoiled, but also unfermented. A week ago I even added some backslop from previous kraut, but that didn't kickstart anything.

I'm gonna need to toss this batch then start a new one, darnitall.
