---
title: sauerkraut
date: 2016-07-30
author: vmbrasseur
excerpt: ""
layout: single
categories:
  - kraut
---
| 900g | green cabbage |
| 118g | red radish |
| 45g | salt |

Using 4% salt because (a) it's been hot and I want to slow down the ferment, and (b) I'll be gone for nearly a month and I want to slow the ferment. So. yeah: Let's seet whether it slows things down.

Weighted down w/a zippy of 4% brine solution.

## 2016-08-04

Started discolouring a little. Radishes now slightly pink tinged rather than showing red. I think the acid must've reached a critical level of some sort but I haven't tested it yet.

## 2016-08-07

Taste test. Smells fairly ripe. A lot of juice. A bit salty and low on the acid, but progressing pretty well.

## 2016-09-03

No. Much no. Between the high heat while I was gone and the high salt, this turned into salty mush. I threw it out. I wouldn't eat this.
