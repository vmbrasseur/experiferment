---
title: soy tempeh
date: 2024-03-10
author: vmbrasseur
excerpt: ""
layout: single
categories:
  - tempeh
---

| 100 g | dried organic soy beans |
|   8 g | [kraut juice](/2023/10/savoy-leek-kraut)
|  .5 g | tempeh starter |

I'm starting my forays into tempeh, and I'm starting small with a 100g batch. I don't know what I would do with a full 1kg or even 500g at this point.

This afternoon (~15:00) I started the beans soaking. I added a large spoonful of kraut juice to acidulate the water and kickstart the short lacto ferment the beans get with the 24-ish hour soak.

## 2024-03-11

<img src="/assets/images/20240311-tempeh.jpeg" alt="A covered plastic bin with sous vide immersion circulator in it. The circulator reads 36.3°C and is set to 30°C. A hand is holding the lid open, revealing a bread tin partially filled with innoculated soy beans." title="A covered plastic bin with sous vide immersion circulator in it. The circulator reads 36.3°C and is set to 30°C. A hand is holding the lid open, revealing a bread tin partially filled with innoculated soy beans." style="width:250px; float:left; margin-right: 5px;" />

OK, it's started! Turns out the plastic bin I got for this is too small for most of my containers (oops), but thankfully my smallest bread tins fit well and made for a just-thick-enough little tempeh-to-be loaflet.

I'm using my Anova sous vide to maintain a humid environment at a constant 30°C. I also have a little temperature sensor in the soybeans so I can track the progress of the ferment.

Now to leave it alone for a while. I'll check it again tomorrow morning.

## 2024-03-12

### Morning

<img src="/assets/images/20240312-tempeh.jpg" alt="A close up of the soy beans. They show little change yet. Maybe a slight fuzziness of the tempeh molds? Maybe?" title="A close up of the soy beans. They show little change yet. Maybe a slight fuzziness of the tempeh molds? Maybe?" style="width:180px; float:left; margin-right: 5px;" />

Checked in on the tempeh after I fed the cats this morning (~07:30). It doesn't look too different yet. There _might_ be a little fuzz starting to grow. Maybe. The temp sensor reads more or less the same as the temp set for the sous vide.

### Evening

<img src="/assets/images/20240312-tempeh-2.jpg" alt="A top-down view of the finished tempeh. It's covered in a layer of white fluffy mold." title="A top-down view of the finished tempeh. It's covered in a layer of white fluffy mold." style="width:200px; float:left; margin-right: 5px;" />
<img src="/assets/images/20240312-tempeh-3.jpg" alt="A side view of the finished tempeh. The soybeans are encased in a firm white mold." title="A side view of the finished tempeh. The soybeans are encased in a firm white mold." style="width:250px; float:left; margin-right: 5px;" />

The sensor started reading increasing temperatures in the afternoon. Eventually it topped out around 42°C and stayed there for a while.

I figured this would be OK, since my brain vaguely recalled that around 50°C is the problem area. I remembered wrong. In the evening when I went to have a peek at the tempeh, I also turned the page of the book I'm using. Right there, in black and white, it says that I shouldn't let it stay warmer than 41°C for more than an hour or it'll kill off the mold and it'll stop growing.

welp.

Despite that, the tempeh looks well developed? It was in an open-top container, so the mold was able to get all fluffy on the top. This is cute, but I admit it's rather off-putting for a foodstuff.

Anyway, I put it in the fridge and plan to try it in a stir fry tomorrow, if my schedule allows.

## 2024-03-17

It took rather longer than desired for me to get to cooking it up. It was in the fridge, but had started to sporulate a bit.

I cut it into cubes, marinated it in ginger-garlic-soy, pan fried it, added to a stir fry.

I did not enjoy it. It was very mushroomy. Every time I walked back into the kitchen that day it reeked of fungus.

Perhaps this was because I allowed it to sporulate. Maybe it's just the way the fresh tempeh tastes.

I may give it another attempt with channa dal or some other bean I enjoy more, but currently I'm not thrilled with the tempeh experiments.
