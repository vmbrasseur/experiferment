---
title: brussels sprouts, collards, leek kraut
date: 2021-03-13
author: vmbrasseur
excerpt: ""
layout: single
categories:
  - kraut
---

<img src="/assets/images/20210313-kraut.jpeg" alt="A close-up picture of the prepared veggies before adding them to the fermenter or adding the brine" title="A close-up picture of the prepared veggies before adding them to the fermenter or adding the brine" style="width:250px; float: left; margin-right: 5px;" />

|      408 g   | sliced brussels sprouts |
|      253 g   | finely sliced collard greens, incl stems |
|      136 g   | finely sliced leek |
|       12 g   | garlic, crushed & medium-chopped|
|    **809 g** | **TOTAL VEG** |
|       48 g   | kraut juice |
|       2.5%   | brine |

I really enjoyed the collard/leek combination in the [napa cabbage, collard green, leek kraut](/kraut/napa-collard-leek-kraut/) and decided to try it again.

This time I added brussels sprouts (because they were on sale) and a little garlic for some extra kick.

None of these veggies are especially juicy, so rather than thumping them to get juice I just packed them in the fermenter, weighed them down, then covered them with abundant 2.5% brine. There's going to be _way_ too much brine when this is all done, so I'll have extra for some other purpose. On the plus side, the chance of any mold reaching the kraut is currently zero (assuming mold makes an appearance; everything was Star-San'd before adding the kraut).

The fermenter this time is a 4 litre Cambro. I made sure the top seals well on this one.

It was 15.5C in the fermentation room when I placed the fermenter down there.

## 2021-03-29

Peeked in on it today. It had blown off into the tray but no mold had snuck in during that, which was nice to see.

Still tastes pretty salty, but it's developing. **pH of 4.1** on this test. Temp in the fermentation room was 14.4C when I was down there.

Stirred the mix up a bit then put it back downstairs. I'll check again in another two weeks.

## 2021-04-13

<img src="/assets/images/20210313-kraut-2.jpeg" alt="Two wide-mouthed pint jars and a wide-mouthed quart jar all filled with completed kraut" title="Two wide-mouthed pint jars and a wide-mouthed quart jar all filled with completed kraut" style="width:250px; float: left; margin-right: 5px;" />

Success! Nary a smidge of mold anywhere at any step in the process.

The kraut pH is **3.9** today. It tastes very good and the veggies still have a nice crunch to them. There's a nice acid bite.

Pretty darn pleased with how this turned out, I gotta say.
