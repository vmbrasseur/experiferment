---
title: jelly grape kefir
date: 2020-01-12
author: vmbrasseur
excerpt: ""
layout: single
categories:
  - drinks
---
<img src="/assets/images/20200112-jellygrapekefir.jpg" alt="A mason jar filled with a honey water kefir in secondary fermentation along with a bunch of very sweet grapes" title="A mason jar filled with a honey water kefir in secondary fermentation along with a bunch of very sweet grapes" style="width:250px; float: left;" />

| 1 batch | water kefir made with local wildflower honey |
| 1/2 cup | [JellyBerries grapes](http://web.archive.org/web/20200113000012/http://divineflavor.com/jellyberries/), cut in half lengthwise |

I picked up these grapes on a whim. They're incredibly sweet. I rinsed them, cut them in half, then dropped them in a batch of honey water kefir for secondary fermentation.

Considering the amount of yeasts that are on grape skins, I expect this to be a very active second fermentation. I'll be interested to see how much (if any) of the grape flavor ends up in the kefir.

## 2020-01-15

Strained it out. No real taste difference from the plain kefir. 
