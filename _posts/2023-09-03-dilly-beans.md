---
title: dilly beans
date: 2023-09-03
author: vmbrasseur
excerpt: ""
layout: single
categories:
  - pickles
---

I didn't weigh anything this time around.

* Fresh dill fronds
* Several cloves of fresh garlic, peeled
* Black mustard seeds
* Black peppercorns
* Several whole cloves
* Two fresh bay leaves from the tree in the yard
* Fresh green beans
* 3% brine

Placed all the herbs/spices in the bottom of a one litre glass jar, packed in the beans top of that, hold it down with a glass weight, pour in the brine and make sure everything is well covered by it.

## 2023-09-12

Checked in on them today. The pH is 4.6. They taste really good and are still crunchy, but could use a little more time to get some acid bite. I'll give them a few more days.

## 2023-09-30

pH is still 4.6. I removed the glass weight then put them in the fridge. They taste pretty good and have a decent consistency. I think they worked out.
