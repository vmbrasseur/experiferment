---
title: pickled turnips
date: 2023-09-10
author: vmbrasseur
layout: single
excerpt: ""
categories:
  - pickles
---

I didn't weigh anything this time around.

* small turnips, sliced into wedges
* small chiogga beet, sliced into rounds
* fresh bay leaf
* two large amaranth leaves
* 3% brine

Placed the slices of beet and bay leaf in the bottom of a big mason jar, then packed the turnips in on top. Poured in the brine, then pressed the amaranth leaves on the top. Placed a glass fermentation weight on the leaves to keep everything well submerged under the brine.

## 2023-09-30

These aren't as pink as I'd prefer, but they taste great and have a good consistency. pH 3.7, which is pretty acidic, and it shows in the taste test. Moved them all into a one pint jar and stowed 'em in the fridge. A success!
