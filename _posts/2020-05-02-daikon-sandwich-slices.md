---
title: daikon sandwich slices
date: 2020-05-02
author: vmbrasseur
excerpt: ""
layout: single
categories:
  - pickles
---

<img src="/assets/images/20200502-daikon.jpg" alt="One quart mason jar, one half quart, each packed with thinly sliced daikon in a 2% brine" title="One quart mason jar, one half quart, each packed with thinly sliced daikon in a 2% brine" style="width:250px; float: left;" />

| 578 g | daikon, peeled, thinly crinkle cut |
| 54 g | shallot, thinly sliced |
| 25 g | ginger, unpeeled, thinly sliced |
| 657 g | TOTAL VEG |
| 2 | dried chilli pepper |
| 2% | brine |

I didn't use this daikon for the [latest kimchi](https://experiferment.vmbrasseur.com/kimchi/granny-smith-kimchi/), so I had to find a purpose for it. I figured it would be good on sandwiches in place of dill pickle slices.

2% brine because the fermentation room has been cool lately.

Packed into the jars, filled with brine, used a chopstick to remove as many air bubbles as possible.

Used the fermentation springs to hold the veg under the brine. Using the drilled wide mouth mason jar tops and some fermentation airlocks.

The fermentation room was 14.5C and 61% humidity (we've had a lot of rain) when I placed the jars there.

## 2020-05-17

First tasting. I expect they'll be close to done. There's a lot of liquid in both jars, to the point that I suspect I may be able to consolidate them to one jar when it's done.

The liquid was slightly cloudy, but that changed to very cloudy as soon as I started mixing thing up a bit. There's not a speck of mold in sight.

The pickles are coming along, but right now there's more bite from the ginger and shallot than the lactic acid. Decent consistency on the pickles, though. Not going mushy.

I'll let these sit for another week or two.

The fermentation room was about 16C. It's been about the same temperature outside lately, and fairly rainy.

## 2020-05-24

Done! There's a really pleasant acid bite to these now. They'll add a lot of interest to a sandwich or similar.

A couple of the slices I tried were starting to trend toward mushiness, so these got popped into the fridge ASAP.

First though, I did end up consolidating the two jars. The two together perfectly filled the quart jar.
