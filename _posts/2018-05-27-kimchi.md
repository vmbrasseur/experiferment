---
title: kimchi
date: 2018-05-27
author: vmbrasseur
excerpt: ""
layout: single
permalink: /2018/05/27/kimchi
categories:
  - kimchi
---
| ~1kg | large daikon |
| ~1.8kg | Napa cabbage |
| 3 bunches | scallions |
| 3.002kg | TOTAL VEG |
| 15% | brine solution |
| 750ml | water |
| 100ml | brown rice flour |
| 101g | chopped garlic |
| 243g | grated ginger |
| 153g | Korean red pepper flakes |

Soak in a 15% brine solution for 3 hours. Rinse and squeeze/drain well.

Slurry from rice flour and water. Cook, cool.

Mix veg, slurry, spices. Innoculate with the last of the previous batch.

## 2018-06-03

Taste test. A little saltier than I'd prefer. Needs more fermentation (not much zing).

## 2018-06-10

Taste test. It's good! Really nice bite and zing. Flavours melded nicely. Gonna put in jars then refrigerate until I get back from the next travel and can give them away.

The overly salty that I noticed with the first taste test was not a problem on the second.

Made 8 pint jars.
