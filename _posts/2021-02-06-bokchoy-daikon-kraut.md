---
title: bok choy & daikon kraut
date: 2021-02-06
author: vmbrasseur
excerpt: ""
layout: single
categories:
  - kraut
---

<img src="/assets/images/20210206-kraut.jpg" alt="2 1-quart mason jars of kraut" title="2 1-quart mason jars of kraut" style="width:250px; float: left;" />

|      677 g | finely sliced bok choy |
|      521 g | grated daikon |
|       49 g | finely sliced Anaheim chili, seeds removed |
|       16 g | very finely minced lemongrass |
|       27 g | very finely sliced scallions, white & green parts |
|        7 g | very finely minced ginger |
|        8 g | minced garlic |
| **1305 g** | **TOTAL VEG** |
|       26 g | kosher salt (2% weight of veg) |
|       28 g | kraut juice |

Mixed the bok choy and daikon, then thumped well. Mixed everything else in by hand afterward.

Packed the kraut into a couple of 1 quart mason jars, then used sanitised kraut springs and fermentation lids. There's plenty of juice, so the springs keep everything submerged nicely.

I chose to add the kraut juice not only because I happened to have it on hand but also because I wanted to use the jar it was in.

## 2021-02-20

Gave it a first check today.

The ferments are clean. Not a spot of mold in sight. I'm feeling pretty happy about that, I gotta say.

The fermentation room is currently **13.3 C**, and it's been pretty cold the past few weeks (snow, even).

The pH on both of the jars is **3.8**. I would like it to get down to about 3.5 or maybe a touch lower if possible.

A quick taste confirmed that it needs more time to do its fermenting thing. This is going to be a good kraut once it's done, though. The lemongrass was especially pleasant in the taste I took.

I'll check again in another 2 weeks. I expect this will be done by then.

## 2021-03-06

One of the two jars had a thin slick of mold across the top of the ferment. Closer inspection revealed the fermentation lid had popped?? Maybe the kraut spring pushed it open? It smelled a bit off and rotting, so I tossed it in the green bin.

The other jar was good and clean, though. The pH was 3.7 but the flavour and consistency was really good. I still like the lemongrass a lot but it's less pleasant when you get a tough bit of it. Overall though, tasty kraut. I'm sad the other jar didn't work out.

I moved the kraut from the 1 quart jar it was in to a 1 pint wide-mouthed one for storage in the fridge. I've been having kraut on the morning toast & hummus and am almost out of kraut, so the timing is good for this one…but not having 2 jars means I should probably start a new batch of something next week.
