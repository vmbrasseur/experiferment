---
title: green cherry tomatoes
date: 2018-12-02
author: vmbrasseur
excerpt: ""
layout: single
categories:
  - pickles
---
| 676g | green tomatoes, whole and rinsed |
| spices per pint jar |
| 1 | bay leaf |
| 1 | lemon slice |
| 1 | cinnamon stick |
| 2 | anise stars |
| 2 | red peppers |
| 2 | garlic cloves, peeled, whole |

5% brine.

Sterilised 4 pint jars

Packed 3 with tomatoes layered with spices.

Packed 4th jar with cabbage chunkes since I had it handy and a jar ready.

Fill each jar with brine, topped with brine-filled zippies.

## 2018-12-15

First taste test. Smell of spices (esp. anise) very strong when brine bag was removed. No mold in sight. Flavour: not acidic at all. Spices taste OK. Slightly bitter. Gonna let them sit a while longer to develop some acid. It's been quite cool in the house, so this could take a while. Also, next time it could probably benefit from a small lump of rock sugar in the bottom. A little sweetness would be nice.

## 2019-02-17

Brine in each jar is clear but there's mold covering the top of each one. I'm going to remove the molded bits then repack into a quart jar with fresh brine then (assuming they're fermented enough) put in the fridge.

The one that I tried tasted strongly of garlic with a nice anise edge. It was briney but not very sout. I did end up putting the jar in the fridge though. I don't think this batch is going to get more sour anytime soon.

The cabbage jar ended up brin a total loss. There were sections that were still firm but most of it was unpleasantly mushy. It wasn't worth saving.
