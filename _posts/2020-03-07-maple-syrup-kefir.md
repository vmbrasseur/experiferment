---
title: maple syrup kefir
date: 2020-03-07
author: vmbrasseur
excerpt: ""
layout: single
categories:
  - drinks
---

| ~4 fl oz | amber maple syrup |
| 3 | pitted & sulfur-free prunes |
| to fill | spring water |

Water kefir needs natural sugars and some minerals, so there's no reason why maple syrup shouldn't work well, right? I've been adding prunes to the honey water kefirs I've been making (more colour, more nutrients), so I plunked them into this batch as well. I didn't see the harm.
