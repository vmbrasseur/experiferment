---
title: green cabbage spice kraut
date: 2024-06-30
author: vmbrasseur
excerpt: ""
layout: single
categories:
  - kraut
---

|  1140 g    | shredded green cabbage |
|    92 g    | finely sliced yellow onion |
| **1232 g** | **TOTAL VEG** |
|    31 g    | salt (2.5%) |
|    10 g    | black mustard seed |
|     8 g    | nigella seed |
|     3 g    | ajwain seed |

I usually don't use spices in my krauts, but I felt like it today. The ajwain is pretty strong stuff, so I only used a little bit.

I put this kraut into the jar from which I'd just removed [the previous one](/2024/05/green-cabbage-shallot-kraut), which still contained some juice from the last batch. Hopefully that'll help kickstart this one so it's done well before I've eaten my way through that last batch.
