---
title: chiogga beet kvass
date: 2020-01-12
author: vmbrasseur
excerpt: ""
layout: single
categories:
  - drinks
---
<img src="/assets/images/20200112-chioggabeetkvass.jpg" alt="A mason jar filled with a new batch of kvass made with Chiogga beets" title="A mason jar filled with a new batch of kvaas made with Chiogga beets" style="width:250px; float: left;" />

| 1 medium | diced chiogga beet |
| to fill | spring water |
| large pinch | salt |

## 2020-01-15

Tasted and there's very little change. Shook up the jar a bit to get the bacteria all mixed in.

## 2020-01-18

Tasted and there's a bit of a difference now. Less salty, a bit of tang. Strained it out and put it in the fridge.

## 2020-01-19

Had the first glass of the kvass. It's slightly salty and fairly beet-y. Not a lot of acid, but it was refreshing enough. I wish it had fermented more but I didn't feel comfortable letting it sit much longer.
