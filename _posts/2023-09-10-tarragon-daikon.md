---
title: tarragon daikon
date: 2023-09-10
author: vmbrasseur
layout: single
excerpt: ""
categories:
  - pickles
---

I didn't weigh anything this time around.

* small purple daikon (from the garden), sliced
* fresh tarragon sprigs
* fresh bay leaf
* black peppercorns
* black mustard seeds
* two large mizuna leaves
* 3% brine

Place the tarragon, bay, peppercorns, and mustard seeds in the bottom of a large mason jar, then pack in the daikon. Poured in the brine, then pressed the mizuna leaves on the top. Placed a glass fermentation weight on the leaves to keep everything well submerged under the brine.

## 2023-09-30

I opened this jar and it immediately smelled off. Like, disgustingly so. pH was around 5.7, much high than the other two batches I started that day. I removed the glass weight and am dumping the lot. 

Dunno what went wrong here. I treated it _exactly_ like the other two from that day. Maybe it's the tarragon? I've never pickled with it before? But that doesn't seem like the right lead to chase.

Anyway, two out of three ain't bad.
