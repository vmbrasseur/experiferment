---
title: purple kraut #2
date: 2022-08-21
author: vmbrasseur
excerpt: ""
layout: single
categories:
  - kraut
---

<img src="/assets/images/20220821-kraut.png" alt="A stainless steel bowl full of red cabbage, purple daikon, and red onions ready to turn into kraut. Next to the big bowl are two smaller yellow ones. The top small bowl holds the backslop from previous kraut and the bottom one holds the 74g salt." title="A stainless steel bowl full of red cabbage, purple daikon, and red onions ready to turn into kraut. Next to the big bowl are two smaller yellow ones. The top small bowl holds the backslop from previous kraut and the bottom one holds the 74g salt." style="width:250px; float: left; margin-right: 5px;" />

|  1333 g   | finely sliced red cabbage |
|   607 g   | grated purple daikon |
|   508 g   | finely sliced red onion |
|   **2448 g** | **TOTAL VEG** |
|    74 g   | salt (3%) |
|    73 g   | backslop from previous purple kraut batch |
|     3%    | brine to cover |

The previous purple kraut is almost all gone, so it's time to start another batch. The last batch turned out _really_ well, so I'm going to try to recreate something similar to it.

I put this batch up in my 5 litre fermentation crock. After thumping the veg there was _almost_ enough juice to cover the stoneware weights. I added extra brine to make sure everything's plenty covered and not at risk of growing molds.

## 2022-09-11

I peeked into the crock and saw some islands of mold floating at the top. Since I had to clean those up, I also took a pH reading. 3.5 already! Wow! That was a fast kraut.

It's also a tasty kraut. It has really good acid and flavour. I think I succeeded in reproducing the previous batch. Go me!

It made 5 pints and another smaller jar, so unless I manage to give a lot of it away I'm going to have good kraut for quite a while.
