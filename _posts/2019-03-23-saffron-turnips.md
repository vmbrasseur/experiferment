---
title: saffron turnips
date: 2019-03-23
author: vmbrasseur
excerpt: ""
layout: single
categories:
  - pickles
---
| 482g | peeled and sliced turnips |
| 5g | yellow mustard seed |
| large pinch | saffron threads |
| 3% | brine |

Mustard seeds in the bottoms of the jar, along with 1/2 of the saffron. Fill/pack half the jar with turnips. Add rest of saffron, then pack in rest of turnips. Fille with brine. Weight down and add bubbles.

## 2019-03-31

The fermentation is quite active now. This morning I had to empty the bubbler because it had blown bring into it. I tasted a turnip and it was good! No acid really yet but nice flavour. I'm traveling next week, so it gets a week unattended. I hope it doesn't go off in that week.

## 2019-04-05

Didn't go off while I was gone! I tasted one today. Better than last week! It has a tiny bit of acid zing to it. I really love the turnip/mustard smell. I'll let this sit for another week.

## 2019-04-13

A bit more bite this week. I think these are probably done. I'm giving half to Fuzzy and Kris, so I'll need to repack these into two smaller jars. That'ss require jar sanitation and maybe a bit of brine of the same strength.

I packed into 2 pint jars, topped up with 3% brine then put the jars in the fridge.
