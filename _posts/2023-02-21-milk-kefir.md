---
title: milk kefir
date: 2023-02-21
author: vmbrasseur
excerpt: ""
layout: single
permalink: /2023/02/milk-kefir
categories:
  - drinks
---

The [last attempt](https://experiferment.vmbrasseur.com/drinks/milk-kefir/) at milk kefir didn't work out. It's been a few years, so I've decided to give it another try.

## 2023-02-21

The kefir grains arrived from [Yemoos](https://www.yemoos.com). I gave them a good rinse with whole milk (from Trader Joe's), then popped them in a jar with about 2/3 cup of the same milk. I placed a paper towel over the top of the jar and closed it with a canning ring.

I got things started just before bed, which wasn't the best timing since it's supposed to ferment for 24 hours. Oops.

## 2023-02-22

I gave it a good stir when I got up this morning. The milk is already starting to taste a bit fermented.

## 2023-02-23

Strained everything last night, placing the kefir in the fridge for the morning. 

I tried the kefir this morning. It was somewhat thicker than normal milk and tasted slightly fermented. There was a slight bitter edge to it.

I think 2/3 cup was too much for these new-to-me grains as they settle into their new environment. The batch that's currently going is only about 1/2 a cup of whole milk.

## 2023-02-24

Last night I decided not to strain it at 24 hours and instead leave it until morning. That'd get it onto a reasonable schedule.

This ended up being a good idea for other reasons. When I strained the kefir this morning, it was much thicker than the first batch. This is more in line with what I've come to expect from store bought kefir. When I tasted it, the bitter edge was gone. The kefir had a nice acid bite, but even better it had a slight effervescence.

The grains have doubled already (if not more). Since there are more of them now, the next batch is 2/3 of a cup of whole milk again.

## 2023-02-25

The kefir was still rather liquidy when I checked around 8am, so I stirred it up and let it sit a while longer. By 2pm it had thickened up so I strained it off into a jar. I'd recently eaten, so I didn't drink it. It'll be dessert or breakfast or something like that.

Refreshed with 1 cup of whole milk.

## 2023-02-26

Stirred first thing in the morning and then again around noon. It was pretty thin each time but I figured it would be thicker by 4-ish, since that's about the same time it sat yesterday. When I checked around 5pm it was starting to split into whey, so I let it go too long.

I strained it, popped it in the fridge for later, then refreshed with 1.25 cups whole milk.

## 2023-02-28

I forgot to update yesterday, so I'll only do today's.

By the time I woke this morning, the kefir was already splitting to whey. I strained it out anyway after stirring it up.

My guess is that it fermented so quickly due to the bits of the previous batch sticking to the kefir grains, so today I rinsed them off in springwater before starting the batch for tomorrow.
