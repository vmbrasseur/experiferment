---
title: nasturtium vinegar
date: 2018-08-12
author: vmbrasseur
excerpt: ""
layout: single
categories:
  - vinegar
---

Fill pint jar with nasturtium flouwers. Add rice wine vinegar to cover. Close tightly and set in cupboard.

## 2018-08-13

Lovely bright orange liquid, rather like hot chili oil.
