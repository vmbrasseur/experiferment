---
title: herb beans
date: 2019-07-07
author: vmbrasseur
excerpt: ""
layout: single
categories:
  - pickles
---

* a couple pounds each of yellow wax beans and green beans (from farmers market)
* large handfulls each dill, tarragon from my garden
* 3% brine
* whole, unpeeled garlic cloves

Put the tarragon in with the yellow beans and the dill with the green. Garlic goes in both.

Packed into the jars as tightly as possible. Garlic and herbs only in the bottom of the jar. StarSan'd all the things. Four jars total, two of each bean. Three of them are using the new fermentation springs instead of the glass weights. I think they'll make it easier to check progress.

## 2019-07-21

Taste test. Yellow: not a lot of tarragon flavour but it's otherwise developing OK. Start of some acid, but I think it needs another couple of weeks. Green: decent dill flavour. Nicely balanced. Otherwise same as yellow but with a touch less acid.

## 2019-07-27

Much more tarragon in yellow now. Also, garlic. Still happy with dill in green. Slightly more acid in green this time (than yellow), but neither have a lot. Salt still predominates. Very far from mushy. Can ferment longer. Try again in 2 weeks.

## 2019-08-11

Mold was starting to form at the top of each jar I sampled. Removed, then sterilized compression springs using boiling water. More acid in each jar but still not where I want them. Getting close though.

## 2020-01-10

These are neglected in their jars. Soon after the last tasting I had to pack up and move so these got ignored. They're now in the storage room in the basement of the house. They make the room smell of garlic. There's a ton of mold on top. I need to dump these and reclaim the gear they're using (compression springs).
