---
title: milk kefir
date: 2019-12-29
author: vmbrasseur
excerpt: ""
layout: single
categories:
  - drinks
---
Got some grains from Yemoos. Followed directions for reviving dried grains.

Have made a small batch nearly every day since. Using supermarket whole milk. Never really tastes right. Gets thicker but not sour at all. Has a bitter-like edge to it. Grains not really growing or changing a lot.

Gonna work my way through this half gallon of milk an then try something a bit less factory-farm.
