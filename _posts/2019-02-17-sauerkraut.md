---
title: sauerkraut
date: 2019-02-17
author: vmbrasseur
excerpt: ""
layout: single
categories:
  - kraut
---
| 739g | green cabbage |
| 18.5g | salt (2.5%) |

This is the test run for the new fermenting gear: StarSan, glass weights, fermentation airlocks.

Also, for the first time I was able to give the kraut a good thumping and packing using the end of my new birch rolling pin.

I'm optimistic.

## 2019-02-23

Lokks very good so far. Removed the glass weight and smells good, sour. Good taste. This is done and headed to the fridge. Woo!
