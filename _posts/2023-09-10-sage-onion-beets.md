---
title: sage and onion beets
date: 2023-09-10
author: vmbrasseur
excerpt: ""
layout: single
categories:
  - pickles
---

I didn't weigh anything this time around.

* small chiogga beets, sliced 
* red onion slice
* fresh thyme sprigs
* fresh sage leaves
* fresh bay leaf
* two large mizuna leaves
* 3% brine

Place the thyme, sage, and bay in the bottom of a large mason jar. Put the slice of onion on top to hold it all down, then pack in the beets. Poured in the brine, then pressed the mizuna leaves on the top. Placed a glass fermentation weight on the leaves to keep everything well submerged under the brine.

## 2023-09-30

I have finally found pickled beets that I like! These taste GREAT. What a huge success. I moved them to a pint jar and stowed them in the fridge. pH was 4.4.

