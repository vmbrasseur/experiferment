---
title: dilly beans
date: 2015-08-23
author: vmbrasseur
excerpt: ""
layout: single
categories:
  - pickles
---
No weights for this one, only ingredients.

| 1 bunch fresh dill |
| green beans |
| garlic cloves, peeled |
| dried red peppers |

Put a lot of dill at the bottom of a quart jar. Add some cloves of garlic and a few peppers. Pack in the green beans.

Cover with 5% brine. Loosely screw on tops. Set aside.

## 2015-08-27

Checked beans. One jar is fine. I'm not tasting any acid yet but they look OK. The other jar is moldy. I'll need to toss it out.
