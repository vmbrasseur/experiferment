---
title: brussels sprouts and radish leaves
date: 2020-12-29
author: vmbrasseur
excerpt: ""
layout: single
categories:
  - pickles
---

<img src="/assets/images/20201229-sprouts.jpg" alt="Quartered Brussels sprouts in an 8 litre Cambro" title="Quartered Brussels sprouts in an 8 litre Cambro" style="width:250px; float: left;" />

| 93 g | whole radish greens
| 961 g | quartered Brussels sprouts
| 1054 g | TOTAL VEG |
|  | 2% brine |

There were three motivators for this:

1. I bought radishes for the posole that I'm making today and wanted to use the greens.
1. Brussels sprouts were only $2 per pound.
1. I didn't have anything fermenting right now.

I want these to be more pickle nibbles than kraut, so I simply quartered the sprouts.

The sprouts are placed on a bed of the radish greens. I expect they'll add a little mustardy bite to things. It's possible they won't be very good at the end, but I'm OK with using them for flavour only then throwing them out at the end if need be.

I considered adding some garlic, but I decided I preferred to go without this time. The garlic can be a bit overpowering sometimes.

After the kraut-splosion from the [napa-collard-leek](./kraut/napa-collard-leek-kraut/) batch, I wised up and put this ferment in my biggest Cambro. I added about 3 litres of 2% brine (made with spring water, not tap) then weighed the whole lot down with one of the pottery weights that Fuzzy made me. The brine comes well over the top of the weight, with all of the veg safely submerged.

I placed the lot in the fermentation room, which was 14C at that time.

## 2021-01-02

Even though it's only been a few days, I peeked in on the sprouts this morning since I happened to be in that room anyway.

Things are progressing well so far. The brine is starting to get cloudy and there are some bubbles floating on the top. It'll probably be several weeks before it's at a decent level of acidity, but this is a promising start.

It's 15C in that room right now, BTW.

## 2021-01-16

The fermentation room has been pretty pungent lately. I expected that with sprouts and radish involved.

When I checked it today there was a slick of mold across the top of the brine. To be safe, I moved all the sprouts to another newly sanitised container and replaced all the brine with a fresh batch of 2%.

While doing that, I tossed out the radish leaves. They didn't really look appealing.

I tasted one of the sprouts, of course. It's developing a nice acid! It has a pleasant bite and is still nice and crunchy.

I'll give it another two weeks. I think it'll be done by that time.

Before I dumped the old brine I gave it a quick test with my spiffy new pH meter. It was at a **4.2**. The fermentation room was at 14.4C when I retrieved the fermentation container.

## 2021-01-30

<img src="/assets/images/20201229-sprouts-2.jpg" alt="Fermented Brussels sprouts in three pint-sized jars" title="Fermented Brussels sprouts in three pint-sized jars" style="width:250px; float: left; margin:5px;" />

All done. 

Nary a spot of mold in sight when I checked the sprouts this morning, which shows that the sanitisation of the gear and the brine swap did their job.

The sprouts still have a decent amount of crunch but they're slightly softer than last time. It's a good balance now. There's a decent acid. In fact, the brine registers a **3.6** on the pH meter.

I packed the sprouts tightly into three pint-sized jars then poured in the brine.

These would probably be really good on a cheese plate.
