---
title: sauerkraut
date: 2019-04-06
author: vmbrasseur
excerpt: ""
layout: single
categories:
  - kraut
---
| 949g | green cabbage |
| 24g | salt (2.5%) |

Now standard StarSan, weight, bubbler, 1qt Mason jar. Got a good thumping before packing into the jar. Hope to ferment this one longer than last time. That one's good but it needs more bite.

## 2019-04-13

Twice now this has blown off into the fermentation airlock. I guess I should be pleased the ferment is active. The flavour is still fairly salty. Definitely needs another week (at least, maybe 2). Another detail: this has been fermenting down in the lower temperatures of the basement.

## 2019-04-20

No blow off into the bubbler this time. It's making good progress but I want this batch to be sharper. Right now it has a nice balance between bite (acid) and salt but I want more on the bite side. It's probably fermenting more slowly because it's in the basement. I'll give it another week.

## 2019-04-27

That escalated quickly. It's definitely sour now. Maybe a tad bit too much, but it's still good. Put it in the fridge today.
